# Documentation secretaire

Documentations des tâches du secrétariat Debian France

--------------------------------------------------------------------------------

## Premier trimestre
- [AGO + Renouvellement CA & Bureau](https://salsa.debian.org/debian-france-team/workshop/documentation-bureau/-/blob/master/Secretaire-ago-ca-bureau.md)

## Suite AGO
- Mise à jour des membres du CA sur la ML ca@
- Mise à jour des membres du CA sur Galette
- Mise à jour des membres du CA sur depot « proces-verbaux_ca » : liste-membres-ca.md
- Lancer appel à volontaire renouvellement Bureau puis lancer les votes
- Mettre copie mail cloture AGO dans depot « proces-verbaux_ag »
- Mettre à jour des mots de passe des différents comptes sur les réseaus sociaux et les envoyer par courriel chiffré aux membres du CA

## Suite élection Bureau
- Mise à jour des membres du Bureau sur la ML bureau@
- Mise à jour des alias president@ tresorier@ et secretaire@
- Faire PV AGO 
- Faive PV Bureau
- Demander au président de faire la déclaration en préfecture
- Revoir et re-voter la délégation des pouvoirs et mettre à jour la liste liste-deletation-pouvoirs.md sur le dépot « proces-verbaux_ca »
- Vérifier et nettoyer les accès [Salsa Debian France](https://salsa.debian.org/groups/debian-france-team/-/group_members) (Bureau : Owner, CA : Maintainer, Membres : Developer) 
- Mettre à jour la page [l'équipe](https://france.debian.net/equipe/) du site avec le nouveau CA et Bureau

--------------------------------------------------------------------------------

## Gestion des membres

Tous les trimestres :
- Relancer les membres qui ne sont pas à jour des cotisations
- Désactiver les comptes ayant eu 3 relances non fructueuses
- Après une durée à définir, supprimer les comptes désactivés

Doc spécifique pour la relance des membres :
- Préparer un courriel (si possible avec Galette),
- Penser à mettre à jour la fiche du membre avec la date de la relance.

Penser à faire la mise à jour avant l'AGO en précisant qu'il est nécessaire d'être à jour pour participer.

--------------------------------------------------------------------------------

## Gestion du serveur / Git

- Vérifier et nettoyer les comptes utilisateurs sur le serveur
- Vérifier et nettoyer les accès « gitolite-admin »
- Vérifier et nettoyer les accès à Gandi
