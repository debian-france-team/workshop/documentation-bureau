Organisation AGO Debian France + Renouvellement CA & Bureau
==========================================================================

## Un mois avant l'AGO - rappeler aux membres de cotiser

* Sur Galette, faire un rappel aux adhérents qui ne sont pas à jour et penser à y inclure ceux qui ne seront pas à jour pour l'AGO.

* Penser à inclure dans le mail qu'ils vont perdre le statut de « Membre » et qu'il ne pourront pas voter.

* Une fois le mois écoulé, retirer le statut « Membre » dans Galette.

## Préparation AGO

### Extraction de la liste membres de Galette

* Dans Galette, sélectionner tous les comptes actifs qui ont un statut différent de « Non membre ».
* Télécharger cette liste.

### Extraire les adresses mails et inscrire tout le monde sur la ML **ag@**

### Extraire les champs suivants pour les injecter dans Limesurvey

+ Prénom,
+ Nom,
+ Courriel.

Le rendu final est donc le suivant (la première ligne est nécessaire) :
```
"Prénom","Nom","Courriel"
"John","Doe","j@doe.fr"
"Due","Mie","mie@molette.fr"
```

Note pour extraire les champs désirés :
```
# Vérifier qu'il n'y a pas de ligne incorrecte (commentaire avec retour à la ligne par exemple) :
grep -vE '^"' fichier_export_galette.csv

# Etraite les chants qui nous désirés :
awk -F';' '{print $4","$3","$17}' fichier_export_galette.csv

# Remplacer la première ligne par :
"firstname","lastname","email"
```

### Création du questionnaire dans Limesurvey

> Vous pouvez exporter un questionnaire de l'année précédente (format .lss par exemple)

1. Créer un nouveau questionnaire
2. importer le fichier précédemment enregistré


## Convocations

Voir article 9.2 des statuts.

> Au plus tard quinze jours avant la date fixée, le secrétaire convoque
> les membres de l'Association par courrier électronique.
> Une copie de cette convocation est adressée à la liste de diffusion de l'Association.
> L'ordre du jour de l'Assemblée Générale est indiqué sur les convocations.

Donc à faire 15 jours avant le début de la phase 1.

Les convocations doivent être envoyées :
* par mail à la ML ag@ (et donc aux adhérents),
* par copie du mail à la ML asso@.

Les convocations doivent inclure les éléments suivants :
* Les dates précises,
* Nombre prévu de votant,
* Rappel sur les cotisations,
* Ordre du jour,
* Liste de membres du CA
* Liste des postes à pourvoir au CA,
* Appel à candidature pour le CA.

## Déroulement de l'AGO

Voir article 9.1 des statuts

+ Phase 1 : phase de discussion - au moins une semaine (7 jours) en période normale, deux semaines (14 jours) en période de vacances scolaires (toutes zones confondues),
+ Phase 2 : phase de vote durant laquelle les différents points de l'ordre du jour nécessitant un vote sont décidés, et pendant laquelle le Conseil d'Administration est renouvelé - au moins 72 heures.

## Phase de vote - Limesurvey

Les membres sont déjà ajoutés dans Limesurvey, il faut créer le vote en deux parties :
* Partie 1 : Vote pour les membres du CA,
* Partie 2 : Approbation du bilan moral et approbation du bilan financier.

## PV de l'AGO

Une fois la phase de vote terminée, il faut rédiger un PV qui sera envoyé sur la ML ag@ et CC asso@ comportant le résultat des votes suivants :
* Résultat vote CA (avec la liste des nouveau membres et la liste complète du CA)
* Résultat du vote sur le rapport morale
* Résultat du vote sur le rapport financier

## Lancer renouvellement Bureau

La suite est de lancer sur ca@ la phase de renouvellement du Bureau :
* appel à candidature Bureau (poste + adjoint),
* vote en lui même
* une fois le vote terminé => PV => envoyé à la banque par le trésorier (avec les statuts) + préfecture par le président.

## Liens

[Statuts - Version 2.1 du 06/04/2022](https://france.debian.net/documents/Statuts_debian_france_2.1.pdf)
