# Documentation présidence

Documentations des tâches de la présidence Debian France

--------------------------------------------------------------------------------

## Premier trimestre
- AGO: elle implique la production du rapport moral et d'activité. La
fin du processus, incluant l'élection du bureau, devrait cocider avec celle de
l'élection du DPL dans un souci de communication.

## Suite AGO
- COnvoquer le plus vite possible la réuion du CA

## Suite élection Bureau
- il faut, dès la réception des PVs, transmettre le nouveau CA
en préfecture, voire le nouveau siège s'il est modifié. Cette déparche
se fait depuis l'espace associatif, service-public.
- écrire un mail de présentation au nouveau DPL
- écrire la micronews pour la DPN, l'article pour linuxfr, voire le blog
de l'asso. debian-publicity relaiera sans doute sur les réseaux sociaux
ainsi que les community managers du CA. Poster aussi sur asso@, debian-user-french@l.d.o
et sur debian-devel-fr@

## Au quotidien

- répondre aux mails sur president@ en mettant en copie le bureau
- dès le premier jour, surveiller les activités du projet Debian francophone
pour recruter d'éventuels futurs administrateurs. Sollicitations plus
personalisées en septembre et relance en janvier- février avant l'AGO ou pendant
la période de discussion.
